const scribble = require("scribbletune");
scribble.setMiddleC(5);

let clip;
clip = scribble.clip({
  notes: ["e2"],
  pattern: "x--x--x-x--x--x-" + "x--x--x-xx-x--x-"
});
scribble.midi(clip, "perc.mid");

clip = scribble.clip({
  notes: ["c2"],
  pattern: "x".repeat(16)
});
scribble.midi(clip, "bass.mid");

clip = scribble.clip({
  notes: ["f2"],
  pattern: "x---".repeat(4)
});
scribble.midi(clip, "kick.mid");

clip = scribble.clip({
  notes: ["a#1"],
  pattern: "x".repeat(16)
});
scribble.midi(clip, "close-hats.mid");

clip = scribble.clip({
  notes: ["f#2"],
  pattern: "--x-".repeat(4)
});
scribble.midi(clip, "open-hats.mid");

clip = scribble.clip({
  notes: ["f2", "d#2", "a#2", "d#2"],
  pattern: "x-x-x-x-".repeat(2)
});
scribble.midi(clip, "rithmic.mid");

clip = scribble.clip({
  notes: ["c1"],
  pattern: "--x__-----------"
});
scribble.midi(clip, "riff.mid");

clip = scribble.clip({
  notes: scribble
    .scale("c", "phrygian")
    .slice(1)
    .reduce((a, b) => a.concat(["c4", b]), []),
  pattern: ("xx---------x--x-" + "xx--------------").repeat(2)
});
scribble.midi(clip, "synth.mid");

clip = scribble.clip({
  notes: "c3",
  pattern: "-x-x-x-x_-xx_-xxxx--xx--xx-xx---"
});
scribble.midi(clip, "riff2.mid");

clip = scribble
  .clip({
    notes: scribble
      .scale("c", "phrygian")
      .slice(1)
      .sort(() => 0.5 - Math.random())
      .reduce((a, b) => a.concat(["c4", b]), []),
    pattern: "x-xx-xx-x-xx-xx-".repeat(2)
  })
  .concat(
    scribble.clip({
      notes: scribble
        .scale("c", "phrygian")
        .slice(2)
        .sort(() => 0.5 - Math.random())
        .reduce((a, b) => a.concat(["c#4", b]), []),
      pattern: "x-xx-xx-x-xx-xx-".repeat(2)
    }),
    scribble.clip({
      notes: scribble
        .scale("c", "phrygian")
        .slice(3)
        .sort(() => 0.5 - Math.random())
        .reduce((a, b) => a.concat(["d#4", b]), []),
      pattern: "x-xx-xx-x-xx-xx-".repeat(2)
    })
  );
scribble.midi(clip, "synthP2.mid");

clip = scribble.clip({
  notes: "e2",
  pattern:
    "x---------------" +
    "x---------------" +
    "x---------------" +
    "x---------------" +
    "x-------x-------" +
    "x-------x-------" +
    "x---x---x---x---" +
    "x---x---x---x---" +
    "x-x-x-x-x-x-x-x-" +
    "x-x-x-x-x-x-x-x-" +
    "xxxxxxxxxxxxxxxx" +
    "xxxxxxxxxxxxxxxx"
});

clip = clip.concat(
  scribble.clip({
    notes: "e2",
    pattern: "x".repeat(32),
    noteLength: 1 / 32
  }),
  scribble.clip({
    notes: "e2",
    pattern: "x".repeat(64),
    noteLength: 1 / 64
  }),
  scribble.clip({
    notes: "e2",
    pattern: "-".repeat(16) + "x---x---x---x-x-"
  })
);

scribble.midi(clip, "rise.mid");
